//
//  MovieDetailsViewController+ViewModel.swift
//  MovieList
//
//  Created by Kranthi Veeramalla on 05/03/20.
//  Copyright © 2020 Kranthi Veeramalla. All rights reserved.
//

import Foundation
import UIKit
// MARK: - MovieDetail Protocol -
protocol MovieDetailDelegate: class {
    func loadMovieDeatails()
}
// MARK: - ViewModel
extension MovieDetailsViewController {
    class ViewModel {
        var delegate: MovieDetailDelegate?
        let movieDetails: MovieDetails!

        init(_ movieDetail: MovieDetails) {
            self.movieDetails = movieDetail
        }
        func setup() {
            delegate?.loadMovieDeatails()
        }
    }
}
