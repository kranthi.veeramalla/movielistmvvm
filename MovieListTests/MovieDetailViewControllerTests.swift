//
//  MovieDetailViewControllerTests.swift
//  MovieListTests
//
//  Created by Kranthi Veeramalla on 06/03/20.
//  Copyright © 2020 Kranthi Veeramalla. All rights reserved.
//

import XCTest
@testable import MovieList
class MovieDetailViewControllerTests: XCTestCase {
    var detailView: MovieDetailsViewController?
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let dataModel =   detailView?.viewModel.movieDetails
        XCTAssertNotNil(dataModel != nil)
        detailView?.loadImage(from: "https://is4-ssl.mzstatic.com/image/thumb/Video123/v4/16/af/78/16af78f8-3f20-3882-442b-34e92030276a/2620216237007.jpg/113x170bb.png")
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let nav1 = UINavigationController()
        let movieDetailVC = MovieDetailsViewController(nibName: "MovieDetailsViewController", bundle: nil)
        nav1.viewControllers = [movieDetailVC]
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = nav1
        detailView?.viewModel.setup()
    }
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    func testEmptyListOfViewModel() {
        let viewModel = detailView?.viewModel
        XCTAssertNotNil(viewModel?.movieDetails.detailImage != nil)
        XCTAssertNotNil(viewModel?.movieDetails.movieCategory != nil)
        XCTAssertNotNil(viewModel?.movieDetails.movieReleaseDate != nil)
        XCTAssertNotNil(viewModel?.movieDetails.movieReleaseDate != nil)
    }
}
