//
//  MovieListTests.swift
//  MovieListTests
//
//  Created by Kranthi Veeramalla on 02/03/20.
//  Copyright © 2020 Kranthi Veeramalla. All rights reserved.
//

import XCTest
@testable import MovieList

class MovieListTests: XCTestCase {
    var movieListViewController: ViewController?
    let vcViewModel: ViewControllerViewModel = ViewControllerViewModel()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        if self.movieListViewController == nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let navigationController = storyboard.instantiateInitialViewController() as? UINavigationController else { return }
            guard let movieListViewController = (navigationController.topViewController as? ViewController) else { return }
            UIApplication.shared.keyWindow!.rootViewController = movieListViewController
            // this is the line responsible for the race condition
            RunLoop.main.run(until: NSDate() as Date)
            let isviewLoad =   movieListViewController.viewIfLoaded
            XCTAssertTrue(isviewLoad != nil)
        }
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        movieListViewController = nil
    }
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        //XCTAssert(movieListViewController!.isViewLoaded)
    }
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    func testParentViewHasSubview() {
        let subviews = movieListViewController!.view.subviews
        XCTAssertTrue(subviews.contains(movieListViewController!.tableView), "View does not have a table subview")
    }
    func testHasATableView() {
        XCTAssertNotNil(movieListViewController?.tableView)
    }
    func testTableViewHasDelegate() {
        XCTAssertNotNil(movieListViewController!.tableView.delegate)
    }
    func testThatTableViewLoads() {
        XCTAssertNotNil(movieListViewController!.tableView, "TableView not initiated")
    }
    func testThatTableViewHasDataSource() {
        XCTAssertNotNil(movieListViewController?.tableView.dataSource, "Table datasource cannot be nil")
    }
    func testThatViewConformsToUITableViewDelegate() {
        XCTAssertTrue(movieListViewController != nil, "View does not conform to UITableView delegate protocol")
    }
    func testTableViewIsConnectedToDelegate() {
        XCTAssertNotNil(movieListViewController?.tableView.delegate, "Table delegate cannot be nil")
    }
    func testTableViewConfromsToDelegateProtocol() {
        XCTAssertTrue(movieListViewController!.conforms(to: UITableViewDelegate.self))
        XCTAssertTrue(movieListViewController!.responds(to: #selector(movieListViewController!.tableView(_:didSelectRowAt:))))
    }
    func testTableViewConformsToDataSourceProtocol() {
        XCTAssertTrue(movieListViewController!.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(movieListViewController!.responds(to: #selector(movieListViewController!.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(movieListViewController!.responds(to: #selector(movieListViewController!.tableView(_:cellForRowAt:))))
    }
    func testSearchResult() {
        vcViewModel.searchMoviesList(with: "Richard")
        testManually()
    }
    func testManually() {
        wait(timeout: 10)
    }
    func wait(timeout: TimeInterval) {
        let expectation = XCTestExpectation(description: "Waiting for \(timeout) seconds")
        XCTWaiter().wait(for: [expectation], timeout: timeout)
    }
    func testAsynchronousURLConnection() {
        let URL = NSURL(string: "https://itunes.apple.com/us/rss/topmovies/limit=50/json")!
        let expectationValue = expectation(description: "GET \(URL)")
        let session = URLSession.shared
        let task = session.dataTask(with: URL as URL) { data, response, error in
            XCTAssertNotNil(data, "data should not be nil")
            XCTAssertNil(error, "error should be nil")
            if let HTTPResponse = response as? HTTPURLResponse,
                let responseURL = HTTPResponse.url,
                let MIMEType = HTTPResponse.mimeType {
                XCTAssertEqual(responseURL.absoluteString, URL.absoluteString, "HTTP response URLequal to originalURL")
                XCTAssertEqual(HTTPResponse.statusCode, 200, "HTTP response status code should be 200")
                 XCTAssertEqual(MIMEType, "text/javascript")
            } else {
                XCTFail("Response was not NSHTTPURLResponse")
            }
            expectationValue.fulfill()
        }
        task.resume()
        waitForExpectations(timeout: task.originalRequest!.timeoutInterval) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            task.cancel()
        }
    }
}
