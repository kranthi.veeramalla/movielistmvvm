//
//  APIManger.swift
//  MovieList
//
//  Created by Kranthi Veeramalla on 03/03/20.
//  Copyright © 2020 Kranthi Veeramalla. All rights reserved.
//

import Foundation

//class APIManager {
//    let movieDataURL = "https://itunes.apple.com/us/rss/topmovies/limit=50/json"
//
//}

typealias CompletionHanlder = (_ response: Any, _ status: Bool) -> Void

private enum HttpCodeType: String {
    case get = "GET"
}

private var baseURL: String = {
    return "https://itunes.apple.com/"
}()

private enum Path: String {
    case moviesList
    func getPath() -> String {
        switch self {
        case .moviesList:
            return baseURL + "us/rss/topmovies/limit=50/json"
        }
    }
}

struct APIManager {
    // MARK: Intialization
private init() {}
    // MARK: - Constants
    static let shared = APIManager()
    // MARK: - GetMovieList
    func getMovieList(handler: @escaping CompletionHanlder) {
        let headers = [
            "content-type": "application/json",
            "accept": "application/json"
        ]
        guard let URL = URL(string: Path.moviesList.getPath()) else { return  }
        var request = URLRequest(url: URL)
        request.allHTTPHeaderFields = headers
        request.httpMethod = HttpCodeType.get.rawValue
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                handler(String(), false)
                return
            }
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                handler(data ?? "null raw data", false)
                return
            }
            do {
                if let responseDictionary: [String : Any] = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : Any] {
                    do {
                        let jsonData = try JSONSerialization.data(withJSONObject: responseDictionary, options: .prettyPrinted)
                        let response = try JSONDecoder().decode(WelcomeToMovies.self, from: jsonData)
                        handler(response.feed, true)
                    }
                    catch {
                        handler(error, false)
                    }
                }
            }
            catch let error {
                print(error)
            }
        }.resume()
    }
}
