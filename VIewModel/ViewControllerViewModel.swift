//
//  ViewControllerViewModel.swift
//  MovieList
//
//  Created by Kranthi Veeramalla on 05/03/20.
//  Copyright © 2020 Kranthi Veeramalla. All rights reserved.
//

import Foundation
import SplunkMint
// MARK: - viewmodel Protocol
protocol ViewControllerViewModelDelegate: ViewController {
    func moviesListDownloaded()
}

class ViewControllerViewModel {

    var delegate: ViewControllerViewModelDelegate?
    var feedObject: Feed?
    var entryObject: [Entry] {
        return feedObject?.entry ?? []
    }
    var filteredObjects: [Entry] = []
    func downloadMoviesList() {
        APIManager.shared.getMovieList { (response, status) in
            if status {
                 Mint.sharedInstance()?.logEvent(withName: "sucess")
            }
            if let albums = response as? Feed {
                self.feedObject = albums
                self.filteredObjects = albums.entry
                self.delegate?.moviesListDownloaded()
            }
        }
    }
    func getMovieDetail(with entryDetails: Entry) -> MovieDetails {
        return MovieDetails(movieTitle: entryDetails.title.label,
                            movieCategory: entryDetails.category.attributes.term,
                            movieReleaseDate: entryDetails.imReleaseDate.attributes.label,
                            movieThumbnailImage: entryDetails.imImage.first!.label,
                            movieSummary: entryDetails.summary.label,
                            detailImage: entryDetails.imImage[2].label)
    }
    public func searchMoviesList(with text: String) {
        self.filteredObjects = entryObject.filter({ (movie) -> Bool in
            return movie.title.label.contains(text)
        })
        if text.isEmpty {
            filteredObjects = entryObject
        }
        self.delegate?.moviesListDownloaded()
    }
}
