//
//  MovieDetailsViewController.swift
//  MovieList
//
//  Created by Kranthi Veeramalla on 02/03/20.
//  Copyright © 2020 Kranthi Veeramalla. All rights reserved.
//

import UIKit
import SplunkMint

class MovieDetailsViewController: UIViewController {
    // MARK: - ViewOutlets
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var poster: UIImageView!
    @IBOutlet private var header: UILabel!
    @IBOutlet private var movieCategory: UILabel!
    @IBOutlet private var movieReleaseDate: UILabel!
    @IBOutlet private var overview: UILabel!
    var movieDetails: MovieDetails!
    var viewModel: ViewModel!
    // MARK: - ViewLLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ViewModel(movieDetails)
        viewModel.delegate = self
        viewModel.setup()
    }
}

extension MovieDetailsViewController: MovieDetailDelegate {
    func loadMovieDeatails() {
        intialSetUp()
        let limitedExtraData = MintLimitedExtraData()
        limitedExtraData.setValue("value", forKey: "key")
        Mint.sharedInstance().logView(withCurrentViewName: "MovieDetailsViewController", extraData: limitedExtraData)
    }
}
 // MARK: - Getting Updated Data
extension MovieDetailsViewController {

    private func intialSetUp() {
        loadImage(from: viewModel.movieDetails.detailImage)
        header.text = viewModel.movieDetails.movieTitle ?? ""
        movieCategory.text = viewModel.movieDetails.movieCategory ?? ""
        movieReleaseDate.text = viewModel.movieDetails.movieReleaseDate ?? ""
        overview.text = viewModel.movieDetails.movieSummary ?? ""
    }

    func loadImage(from imageString: String?) {
        guard
            let imageString = imageString,
            let url = URL(string: imageString),
            let data = NSData(contentsOf: url) else {
                poster.image = nil
                return
        }
        poster.image = UIImage(data: data as Data)
    }
}
