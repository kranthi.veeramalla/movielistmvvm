//
//  MovieTableViewCell.swift
//  MovieList
//
//  Created by Kranthi Veeramalla on 03/03/20.
//  Copyright © 2020 Kranthi Veeramalla. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    // MARK: - TableViewCell Outlets
    @IBOutlet  var titleLabel: UILabel!
    @IBOutlet  var categoryLabel: UILabel!
    @IBOutlet private var releasedate: UILabel!
    @IBOutlet  var poster: UIImageView!

    var movieDetails: MovieDetails?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
// MARK: - Content TableViewCell
extension MovieTableViewCell {

    func customizeCell(with movieDetails: MovieDetails) {
        if let imageUrl = movieDetails.movieThumbnailImage, let thumbURL = URL(string: imageUrl) {
            poster.dowloadFromServer(url: thumbURL)
        }
        titleLabel.text = movieDetails.movieTitle
        categoryLabel.text = movieDetails.movieCategory
        releasedate.text = movieDetails.movieReleaseDate
    }
    func loadThumbnail(from imageString: String?) {
        guard
            let imageString = imageString,
            let url = URL(string: imageString),
            let data = NSData(contentsOf: url) else {
                poster.image = nil
                return
        }
        poster.image = UIImage(data: data as Data)
    }
}
 // MARK: - ImageDownload
extension UIImageView {
    func dowloadFromServer(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async {
                self.contentMode = mode
                self.image = image
            }
            }.resume()
    }
}
