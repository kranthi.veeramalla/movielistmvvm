//
//  ViewController.swift
//  MovieList
//
//  Created by Kranthi Veeramalla on 02/03/20.
//  Copyright © 2020 Kranthi Veeramalla. All rights reserved.
//

import UIKit
import SplunkMint
struct MovieDetails {
    let movieTitle: String?
    let movieCategory: String?
    let movieReleaseDate: String?
    let movieThumbnailImage: String?
    let movieSummary: String?
    let detailImage: String?
}

class ViewController: UIViewController, UISearchResultsUpdating {
    var indicator = UIActivityIndicatorView()
    private var viewModel: ViewControllerViewModel?
    let searchController: UISearchController = UISearchController(searchResultsController: nil)
    @IBOutlet weak var tableView: UITableView!
    // MARK: - ViewLLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    private func setUpView() {
        viewModel = ViewControllerViewModel()
        viewModel?.delegate = self
        Mint.sharedInstance()?.logEvent(withName: "setUpView", logLevel: .init(0))

        // Do any additional setup after loading the view.
        self.title = "iTunes Movies"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "MovieTableViewCell")
        viewModel?.downloadMoviesList()
        intializeSearchBar()
        showActivityIndicator()
    }
    func showActivityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.style = UIActivityIndicatorView.Style.medium
        indicator.center = self.view.center
        indicator.backgroundColor = .white
        self.view.addSubview(indicator)
        indicator.startAnimating()
    }
    func hideActivityIndicator() {
        self.indicator.stopAnimating()
        self.indicator.removeFromSuperview()
    }
    // MARK: - intializingSearchBar
    private func intializeSearchBar() {
        DispatchQueue.main.async {
            self.navigationItem.searchController = self.searchController
            self.searchController.searchResultsUpdater = self
            self.navigationItem.hidesSearchBarWhenScrolling = false
            self.searchController.obscuresBackgroundDuringPresentation = false
            self.searchController.searchBar.placeholder = "Movie Title"
            self.navigationItem.hidesSearchBarWhenScrolling = false
            self.definesPresentationContext = true
        }
    }
}
 // MARK: - UITableViewDataSource, UITableViewDelegate -
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.filteredObjects.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell",
                                                       for: indexPath) as? MovieTableViewCell else {
                                                        fatalError("Unable to Dequeue Reusable Table View Cell")
        }
        guard let viewModelObj = viewModel else { return UITableViewCell() }
        let entry = viewModelObj.filteredObjects[indexPath.row]
        cell.customizeCell(with: viewModelObj.getMovieDetail(with: entry))
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  CGFloat(100.0)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModelObj = viewModel else { return }
        let entry = viewModelObj.filteredObjects[indexPath.row]
        Mint .sharedInstance()?.logEvent(withName: "SelectedRowAtTableView")
        let movieDetailVC = MovieDetailsViewController(nibName: "MovieDetailsViewController", bundle: nil)
        movieDetailVC.movieDetails = viewModelObj.getMovieDetail(with: entry)
        self.navigationController?.pushViewController(movieDetailVC, animated: true)
    }
}

extension ViewController: ViewControllerViewModelDelegate {
    func moviesListDownloaded() {
        DispatchQueue.main.async {
            self.hideActivityIndicator()
            self.tableView.reloadData()
        }
    }
}
// MARK: - UISearchResultsUpdating
extension ViewController {
    func updateSearchResults(for searchController: UISearchController) {
        Mint .sharedInstance()?.logEvent(withName: "SearchResult")
        DispatchQueue.main.async {
            self.viewModel?.searchMoviesList(with: self.searchController.searchBar.text!)
        }
       mintTranctiondata(searchTerm: self.searchController.searchBar.text!)
    }
    // MARK: - Add custom data to transactions
    func mintTranctiondata(searchTerm: String) {
        let transactionName: String = searchTerm
        var transactionID: String?
        let mintShare = Mint.sharedInstance()
        transactionID = mintShare?.transactionStart(transactionID, extraDataKey: "", extraDataValue: "")
         Mint.sharedInstance().transactionStop(transactionID, extraDataKey: "key1", extraDataValue: "value1")
         let limitedExtraData = MintLimitedExtraData()
         limitedExtraData.setValue("value2", forKey: "key2")
         transactionID = Mint.sharedInstance().transactionStart(transactionName, extraData: limitedExtraData)
         Mint.sharedInstance().transactionStop(transactionID, extraData: limitedExtraData)
         transactionID = Mint.sharedInstance().transactionStart(transactionName, extraData: limitedExtraData)
         Mint.sharedInstance().transactionCancel(transactionID, reason: "reason")
    }
}
